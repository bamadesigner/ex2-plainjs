( function( wp ) {

    var el = wp.element.createElement;
    var __ = wp.i18n.__;
    wp.blocks.registerBlockType( 'learn-gutenberg/ex2-plainjs', {
        title: __( 'Learn Gutenberg Example 2: Plain JS', 'learn-gutenberg' ),
        category: 'widgets',
        supportHTML: false,
        attributes: {
            who: {
                type: 'string',
                selector: 'p',
                attribute: 'who',
            },
        },
        edit: function( props ) {
            var attributes = props.attributes,
                setAttributes= props.setAttributes,
                className = props.className,
                focus = props.focus,
                id = props.id;

            //set default
            if( ! attributes.hasOwnProperty( 'who' ) ){
                setAttributes( { who: __( 'Roy', 'ex2-plainjs' ) } )
            }

            //Change event for form input
            var whoChange = function (event){
                setAttributes( { who:event.target.value} );
            };

            //Create block UI using WordPress createElement
            return el(
                'div',
                { className: className },
                [

                    el(
                        'p',
                        {
                            who: attributes.who
                        },
                        __( 'Who', 'ex2-plainjs' ) + ': ' + attributes.who
                    ),
                    el(
                        'div',
                        {

                        },
                        [
                            el(
                                'label',
                                {
                                    for: id + '-control'
                                },
                                __( 'Who', 'ex2-plainjs' )
                            ),
                            el(
                                'input',
                                {
                                    id: id + '-control',
                                    value: attributes.who,
                                    onChange: whoChange
                                }
                            ),
                        ]

                    ),

                ]
            );
        },
        save: function( props, className) {
            var attributes = props.attributes;
            var who = attributes.who || 'no one at all';
            return el(
                'p',
                {
                    className:className,
                    who: attributes.who
                },
                __( 'Who', 'ex2-plainjs' ) + ': ' + who,
            );
        }
    } );
} )(
    window.wp
);